Hello!
Congrats on making it this far in the interview process at fjorge—you should be really proud of the job you've done. 

The next step in the process is to complete this development activity.

_________________________________________________________


Ground Rules:

1. Do not share this repository with anyone, as it is owned by fjorge. We want to see what you (and whatever you google on the internet) can come up with. And also, we want you to be able to have conversation on your code decisions.
2. If something is unclear with this activity, ask your contact at fjorge for clarification—we believe in asking questions.
3. Have fun!


Process:

1. Fork this repository as your own private Bitbucket repository. (https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/)
2. Review the XD designs through this weblink: https://xd.adobe.com/view/d18f336f-9717-4ea4-a962-897462c2c80e-84dc/grid
3. Download any necessary assets from the XD link, and load in any relevant font(s) from google fonts. We've included one font in this folder to source in.
4. Download Slick slider to use as your slider. (https://kenwheeler.github.io/slick/)
5. Recreate the single web page from the provided designs.
6. Once you feel happy with your work, push your changes to your repository.
7. Send a zip version of your repo to your contact at Fjorge. 


Requirements:

1. Must be responsive (Both desktop and Mobile views should be as close to pixel perfect as you can get)
2. Must utilize a stylesheet, index file, slickslider, and js file.
3. Show us your skills - use whatever libraries or tools you feel comfortable with or excited about. 


Ideas of what to show off:

* Do you have animations you want to show off? Fade-ins? Hover styles? hamburger menu animation?
* Do you know how to write Sass (using mixins, variables, etc), and compile it?
* Do you know how to use a grid? 
* ADA standards 
* Document your thought process in comments or the readme
** We don't need to see all of these to be impressed with your code, but show us where you're at!


Answers to FAQ:

1. For any links, just add a # as the href
2. Can I use libraries or additional tools? Yep! Feel free to use anything that shows off your best code, just be prepared to talk through your decision.

Next Step:

1. During your interview with a developer at fjorge, you'll walk us through the decisions you made during this activity